const buttonsOrder = document.querySelectorAll(".product__button_order");
const overlayOrder = document.querySelector(".overlay_order");

buttonsOrder.forEach(button => button.addEventListener("click", () => {
    overlayOrder.classList.add("overlay_active");
    const order = overlayOrder.querySelector(".modal__order");
    order.value = button.dataset.order;
}));

overlayOrder.addEventListener("click", event => {
    if (event.target === event.currentTarget || event.target.closest(".modal__close")) {
        overlayOrder.classList.remove("overlay_active");
    }
})