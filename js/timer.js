// https://codepen.io/Quper/pen/zYGxbJm
const declOfNum = (n, titles) => n + ' ' + titles[n % 10 === 1 && n % 100 !== 11 ?
    0 : n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20) ? 1 : 2];

const timer = () => {
    const timer = document.createElement("div");
    const timerText = document.createElement("p");
    const timerCount = document.createElement("span");
    
    timer.classList.add("timer");
    timerText.classList.add("timer__text");
    timerCount.classList.add("timer__count");
    
    timerText.textContent = "Осталось: ";
    
    timerText.append(timerCount);
    timer.append(timerText);
    document.body.prepend(timer);

    const deadline = new Date();
    deadline.setDate(deadline.getDate() + 1);

    const startTimer = () => {
        
        const now = new Date();
        const timeRemain = (deadline - now) / 1000;

        console.log(timeRemain);

        const seconds = Math.floor(timeRemain % 60);
        const minutes = Math.floor(timeRemain / 60  % 60);
        const hours   = Math.floor(timeRemain / 60 / 60 % 24);
        const days    = Math.floor(timeRemain / 60 / 60 / 24 % 30);

        const s = declOfNum(seconds, ["секунда", "секунды", "секунд"]);
        const m = declOfNum(minutes, ["минута", "минуты", "минут"]);
        const h = declOfNum(hours, ["час", "часа", "часов"]);
        const d = declOfNum(days, ["день", "дня", "дней"]);

        timerCount.textContent = `${d} ${h} ${m} ${s}`;

        if (timeRemain > 0) {
            setTimeout(startTimer, 1000);
        } else {
            timer.remove();
        }
    };

    startTimer();
}

timer();